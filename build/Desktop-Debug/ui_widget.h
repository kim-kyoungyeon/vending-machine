/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QWidget
{
public:
    QWidget *VendingMachine;
    QLCDNumber *Coin;
    QPushButton *pb_coffee;
    QPushButton *pb_cola;
    QPushButton *pb_tea;
    QPushButton *pb_reset;

    void setupUi(QWidget *QWidget)
    {
        if (QWidget->objectName().isEmpty())
            QWidget->setObjectName("QWidget");
        QWidget->resize(800, 600);
        VendingMachine = new QWidget(QWidget);
        VendingMachine->setObjectName("VendingMachine");
        VendingMachine->setGeometry(QRect(60, -10, 401, 501));
        Coin = new QLCDNumber(VendingMachine);
        Coin->setObjectName("Coin");
        Coin->setGeometry(QRect(120, 80, 241, 151));
        pb_coffee = new QPushButton(VendingMachine);
        pb_coffee->setObjectName("pb_coffee");
        pb_coffee->setGeometry(QRect(240, 270, 88, 29));
        pb_cola = new QPushButton(VendingMachine);
        pb_cola->setObjectName("pb_cola");
        pb_cola->setGeometry(QRect(240, 320, 88, 29));
        pb_tea = new QPushButton(VendingMachine);
        pb_tea->setObjectName("pb_tea");
        pb_tea->setGeometry(QRect(230, 370, 88, 29));
        pb_reset = new QPushButton(VendingMachine);
        pb_reset->setObjectName("pb_reset");
        pb_reset->setGeometry(QRect(250, 420, 88, 29));

        retranslateUi(QWidget);

        QMetaObject::connectSlotsByName(QWidget);
    } // setupUi

    void retranslateUi(QWidget *QWidget)
    {
        QWidget->setWindowTitle(QCoreApplication::translate("QWidget", "Widget", nullptr));
        pb_coffee->setText(QCoreApplication::translate("QWidget", "PushButton", nullptr));
        pb_cola->setText(QCoreApplication::translate("QWidget", "PushButton", nullptr));
        pb_tea->setText(QCoreApplication::translate("QWidget", "PushButton", nullptr));
        pb_reset->setText(QCoreApplication::translate("QWidget", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QWidget: public Ui_QWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
