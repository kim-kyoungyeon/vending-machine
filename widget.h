#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pb_100_clicked();
    void on_pb_50_clicked();
    void on_pb_10_clicked();
    void on_pb_500_clicked();

    void on_pb_coffee_clicked();
    void on_pb_tea_clicked();
    void on_pb_cola_clicked();
    void on_pb_reset_clicked();



private:
    Ui::Widget *ui;
    QMessageBox m;
    enum Item{
        COFFEE =100,
        COLA = 200,
        TEA = 150
    };

    int money{0};
    void setAvailable();
    void insertMoney(int money);
    void dispenseItem(Item item);
};
#endif // WIDGET_H
