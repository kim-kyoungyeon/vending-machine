#include"widget.h"
#include"ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    setAvailable();
}

Widget::~Widget()
{
    delete ui;
}
void Widget::setAvailable()
{
    ui-> pb_coffee->setAvailabe(money>=100);
    ui-> pb_cola->setAvailabe(money>=150);
    ui-> pb_tea->setAvailabe(money>=200);
}
void Widget::InsertMoney(int money){
    this->money +=money;
    setAvailable();
}
void Widget::dispenseItem(Item item){
//money 참조 this

    auto diff = this->money-item;

    if (diff<0){
        return;
    }
    this ->money = diff;
    setAvailable();

}
void Widget::on_pb_10_clicked(){
    this->insertMoney(10);
}
void Widget::on_pb_50_clicked(){
    this->insertMoney(50);
}
void Widget::on_pb_100_clicked(){
    this->insertMoney(1500);
}
void Widget::on_pb_500_clicked(){
    this->insertMoney(500);
}
void Widget::on_pb_coffee_clicked(){
    this->dispenseItem(COFFEE);
}
void Widget::on_pb_cola_clicked(){
    this->dispenseItem(COLA);
}
void Widget::on_pb_tea_clicked(){
    this->dispenseItem(TEA);
}
void Widget::on_pb_reset_clicked(){
    this->money = 0;
    setAvailable();
    this ->m.information(nullptr,"RESET","PLZ INSERT COIN MORE")

}
